import { Model } from 'objection'
import { User, BulletModel, TYPE, STATUS, SIGNIFIER } from './index'

export default class DailyLog extends BulletModel {
  readonly id!: number
  // inherits common properties from BulletModel
  date: Date
  createdAt: Date
  updatedAt: Date

  user?: User

  static tableName = 'dailyLog'

  static relationMappings = {
    users: {
      relation: Model.BelongsToOneRelation,
      modelClass: `${__dirname}/user`,
      join: {
        from: 'dailyLog.userId',
        to: 'users.id'
      }
    }
  }

  static jsonSchema = {
    type: 'object',
    required: ['title', 'type', 'status', 'signifier', 'date'],

    properties: {
      id: { type: 'integer' },
      userId: { type: 'integer' },
      title: { type: 'string' , minLength: 1, maxLength: 255 },
      description: { type: 'string' },
      date: { type: 'date' },
      type: { type: 'string', enum: TYPE},
      status: { type: 'string', enum: STATUS},
      signifier: { type: 'string', enum: SIGNIFIER}
    }
  }
}