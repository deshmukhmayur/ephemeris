import { Model } from 'objection'
import { User, BulletModel, TYPE, STATUS, SIGNIFIER } from './index'

export default class MonthlyLog extends BulletModel {
  readonly id!: number
  // inherits common properties from BulletModel
  month: Date
  createdAt: Date
  updatedAt: Date

  user?: User

  static tableName = 'monthlyLog'

  static relationMappings = {
    users: {
      relation: Model.BelongsToOneRelation,
      modelClass: `${__dirname}/user`,
      join: {
        from: 'monthlyLog.userId',
        to: 'users.id'
      }
    }
  }

  static jsonSchema = {
    type: 'object',
    required: ['title', 'type', 'status', 'signifier', 'month'],

    properties: {
      id: { type: 'integer' },
      userId: { type: 'integer' },
      title: { type: 'string' , minLength: 1, maxLength: 255 },
      description: { type: 'string' },
      month: { type: 'date' },
      type: { type: 'string', enum: TYPE},
      status: { type: 'string', enum: STATUS},
      signifier: { type: 'string', enum: SIGNIFIER}
    }
  }
}