import { Model, RelationMappings } from 'objection'
import { User, DailyLog } from './index'

export class BulletModel extends Model {
  title: string
  description: string
  type: string
  status: string
  signifier: string
}

export const TYPE = ['task', 'event', 'note']
export const STATUS = ['none', 'complete', 'migrated', 'scheduled']
export const SIGNIFIER = ['none', 'priority', 'inspiration', 'explore']