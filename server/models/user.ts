import { Model } from 'objection'
import { createHmac, randomBytes } from 'crypto'
import { DailyLog, MonthlyLog } from './index'

export default class User extends Model {
  readonly id!: number
  name: string
  email: string
  username: string
  password: string
  passwdSalt: string

  dailyLogs?: DailyLog[]
  monthlyLogs?: MonthlyLog[]

  static tableName = 'users'

  static relationMappings = {
    dailyLogs: {
      relation: Model.HasManyRelation,
      modelClass: `${__dirname}/daily-log`,
      join: {
        from: 'users.id',
        to: 'dailyLog.userId'
      }
    },
    monthlyLogs: {
      relation: Model.HasManyRelation,
      modelClass: `${__dirname}/monthly-log`,
      join: {
        from: 'users.id',
        to: 'monthlyLog.userId'
      }
    }
  }

  /** Default length = 16 */
  public static getSalt(length: number = 16) {
    return randomBytes(Math.ceil(length / 2))
      .toString('hex')
      .slice(0, length)
  }

  public static hashPassword(password: string, salt: string): string {
    const hash = createHmac('sha512', salt)
    hash.update(password)
    return hash.digest('hex')
  }

  public async verifyCredentials(password: string): Promise<boolean> {
    const user = await User.query().where('username', this.username).first()
    if (user) {
      const passwd = User.hashPassword(password, this.passwdSalt)
      return passwd === this.password
    }
    return false
  }
}