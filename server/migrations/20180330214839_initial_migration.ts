import * as Knex from 'knex'

exports.up = (knex: Knex) => {
  return knex.schema
    .createTable('users', table => {
      table.increments('id').primary()
      table.string('name')
      table
        .string('email', 48)
        .unique('email')
      table
        .string('username', 24)
        .unique('username')
      table.string('password')
      table.string('passwdSalt')
      table.timestamp('registrationTime').defaultTo(knex.fn.now())
      table.timestamps(true, true)
    })
    // .createTable('bullets', table => {
    //   table.increments('id').primary()
    //   table.string('title')
    //   table.text('description')
    //   table.enum('type', ['task', 'event', 'note'])
    //   table.enum('status', ['none', 'complete', 'migrated', 'scheduled'])
    //   table.enum('signifier', ['none', 'priority', 'inspiration', 'explore'])
    //   table.dateTime('createdAt')
    //   table.dateTime('updatedAt')
    //   table
    //     .integer('userId')
    //     .unsigned()
    //     .references('id')
    //     .inTable('users')
    // })
    .createTable('dailyLog', table => {
      table.increments('id').primary()
      table.string('title')
      table.text('description')
      table.enum('type', ['task', 'event', 'note'])
      table.enum('status', ['none', 'complete', 'migrated', 'scheduled'])
      table.enum('signifier', ['none', 'priority', 'inspiration', 'explore'])
      table.date('date')
      // table
      //   .integer('bulletId')
      //   .unsigned()
      //   .references('id')
      //   .inTable('bullets')
      table
        .integer('userId')
        .unsigned()
        .references('id')
        .inTable('users')
    })
    .createTable('monthlyLog', table => {
      table.increments('id').primary()
      table.string('title')
      table.text('description')
      table.enum('type', ['task', 'event', 'note'])
      table.enum('status', ['none', 'complete', 'migrated', 'scheduled'])
      table.enum('signifier', ['none', 'priority', 'inspiration', 'explore'])
      table.date('month')
      // table
      //   .integer('bulletId')
      //   .unsigned()
      //   .references('id')
      //   .inTable('bullets')
      table
        .integer('userId')
        .unsigned()
        .references('id')
        .inTable('users')
    })
}

exports.down = (knex: Knex): Knex.SchemaBuilder => {
  return knex.schema
    .dropTableIfExists('monthlyLog')
    .dropTableIfExists('dailyLog')
    // .dropTableIfExists('bullets')
    .dropTableIfExists('users')
}
