import { NextFunction, Request, Response, Router } from 'express'
import { DailyLog, User } from '../models'
import { TYPE, STATUS, SIGNIFIER } from '../models/bullet'

class DailyLogApi {
  public router: Router

  constructor() {
    this.router = Router()
    this.init()
  }

  private init() {
    this.router.get('/', this.getAll)
    this.router.post('/', this.createBullet)
    this.router.patch('/:id', this.updateBullet)
    this.router.delete('/:id', this.deleteBullet)
  }

  private getAll(req: Request, res: Response, next: NextFunction) {
    User.query().findOne(req.user).eager('dailyLogs')
      .then((user: User) => {
        res.status(200)
          .json({
            data: user.dailyLogs,
            error: []
          })
      })
      .catch((err: any) => {
        res.status(err.statusCode || 500)
          .json({
            error: err.message,
            data: []
          })
      })
  }

  private createBullet(req: Request, res: Response, next: NextFunction) {
    const bullet = {
      title: req.body.title,
      description: req.body.description,
      type: TYPE[req.body.type],
      status: STATUS[req.body.status],
      signifier: SIGNIFIER[req.body.signifier],
      date: new Date(req.body.date)
    }

    req.user.$relatedQuery('dailyLogs')
      .insert(bullet)
      .then((insertedBullet: User) => {
        res.status(201).json({
          data: insertedBullet,
          error: []
        })
      })
      .catch((err: any) => {
        console.log(err)
        res.status(err.statusCode || 500)
          .json({
            error: err.message,
            data: []
          })
      })
  }

  private async updateBullet(req: Request, res: Response, next: NextFunction) {
    if (Object.keys(req.body).length !== 1) {
      res.status(400)
        .json({
          error: 'Invalid PATCH request',
          data: []
        })
      return
    }

    await req.user.$relatedQuery('dailyLogs')
      .findById(req.params.id)
      .then((bullet: any) => {
        if (!bullet) {
          res.status(404).json({
            error: `Bullet with id ${req.params.id} does not exist.`,
            data: []
          })
          return
        }
      })

    req.user.$relatedQuery('dailyLogs')
      .patchAndFetchById(req.params.id, req.body)
      .then((bullet: any) => {
        res.status(200)
          .json({
            data: bullet,
            error: []
          })
      })
      .catch((err: any) => {
        res.status(err.statusCode || 500)
          .json({
            error: err.message,
            data: []
          })
      })
  }

  private async deleteBullet(req: Request, res: Response, next: NextFunction) {
    await req.user.$relatedQuery('dailyLogs')
      .findById(req.params.id)
      .then((bullet: any) => {
        if (!bullet) {
          res.status(404).json({
            error: `Bullet with id ${req.params.id} does not exist.`,
            data: []
          })
        }
      })

    req.user.$relatedQuery('dailyLogs')
      .deleteById(req.params.id)
      .then(() => {
        res.status(200)
          .json({
            data: `Deleted ${req.params.id}`,
            error: []
          })
      })
      .catch((err: any) => {
        res.status(err.statusCode)
          .json({
            data: [],
            error: err.message
          })
      })
  }
}

export default new DailyLogApi().router