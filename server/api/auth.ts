import { NextFunction, Request, Response, Router } from 'express'
import * as jwt from 'jsonwebtoken'
import * as objection from 'objection'
import { User } from '../models'
import { jwtOptions } from '../passport.config'

class AuthApi {
  public router: Router

  constructor() {
    this.router = Router()
    this.init()
  }

  private init() {
    this.router.post('/signin', this.signIn)
    this.router.post('/signup', this.signUp)
    this.router.post('/fbsignin', this.facebookLogin)
  }

  private async signIn(req: Request, res: Response, next: NextFunction) {
    const username = req.body.username
    const password = req.body.password
    // console.log(username, password)
    // console.log(req.connection.remoteAddress)
    if (!username || !password) {
      res.status(400).json({
        error: 'Both username and password fields are required'
      })
      return
    }

    const user = await User.query().where('username', username).first()
    if (user && await user.verifyCredentials(password)) {
      const payload = {
        id: user.id,
        clientAddress: req.ip
      }
      const token = jwt.sign(payload, jwtOptions.secretOrKey)
      res.status(200).json({
        token: token,
      })
    } else {
      res.status(400).json({
        error: 'Incorrect username or password'
      })
    }
  }

  private signUp(req: Request, res: Response, next: NextFunction) {
    const name = req.body.fullName
    const email: string = req.body.email
    const password = req.body.password
    const username = email.split('@')[0]

    const salt = User.getSalt()
    const user = User.query()
      .insert({
        name: name,
        email: email,
        username: username,
        password: User.hashPassword(password, salt),
        passwdSalt: salt
      })
      .then(user => {
        res.status(201).json({
          data: user,
        })
      })
      .catch(err => {
        // console.log(err)
        res.status(409).json({
          data: [],
          error: `A user with email '${email}' already exists`,
        })
      })
  }

  private facebookLogin(req: Request, res: Response, next: NextFunction) {
    res.status(200).json({
      message: 'fb login'
    })
  }
}

export default new AuthApi().router