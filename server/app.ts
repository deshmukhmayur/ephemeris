import * as bodyParser from 'body-parser'
import * as express from 'express'
import * as Knex from 'knex'
import * as morgan from 'morgan'
import { Model } from 'objection'
import * as passport from 'passport'
import User from './models/user'

import * as dotenv from 'dotenv'
dotenv.config({
  path: __dirname + '/.env'
})

import * as passportConfig from './passport.config'

import { AuthApi, DailyLogApi, MonthlyLogApi } from './api'

class App {
  public express: express.Application
  public knex: Knex

  constructor() {
    this.express = express()
    this.middleware()
    this.configDb()
    this.api()
  }

  private middleware() {
    this.express.use(bodyParser.json())
    this.express.use(bodyParser.urlencoded({
      extended: true
    }))
    if (process.env.NODE_ENV !== 'prod' &&
      process.env.NODE_ENV !== 'test') {
      this.express.use(morgan('dev'))
    }
    this.express.set('json spaces', 2)

    this.express.use(passport.initialize())

    this.express.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
      err.status = 404
      next(err)
    })
  }

  private configDb() {
    const environment = process.env.NODE_ENV || 'development'
    const knexConfig = require('./knexfile')[environment]
    this.knex = Knex(knexConfig)

    /**
     * TODO: Need some mechanism for migrating the database if there are no migrations
     */
    // await this.knex.migrate.latest({
    //   directory: __dirname + '/knexfile.ts'
    // })

    Model.knex(this.knex)
  }

  private api() {
    this.express.get('/', (req: express.Request, res: express.Response) => {
      res.json({
        message: 'Home',
        ip: req.ip
      })
    })
    this.express.use('/auth', AuthApi)
    this.express.use('/api/v1/daily',
      passport.authenticate('jwt', { session: false, failWithError: true }),
      DailyLogApi,
      (err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
        res.json(err)
      })
    this.express.use('/api/v1/monthly',
      passport.authenticate('jwt', { session: false, failWithError: true }),
      MonthlyLogApi,
      (err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
        res.json(err)
      })
  }
}

export default new App().express