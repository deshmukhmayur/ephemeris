// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      database: process.env.DB_NAME_DEV,
      user:     process.env.DB_USER_DEV,
      password: process.env.DB_PASSWORD_DEV
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  test: {
    client: 'mysql2',
    connection: {
      database: process.env.DB_NAME_TEST,
      user:     process.env.DB_USER_TEST,
      password: process.env.DB_PASSWORD_TEST
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'mysql2',
    connection: {
      database: process.env.DB_NAME_PROD,
      user:     process.env.DB_USER_PROD,
      password: process.env.DB_PASSWORD_PROD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

}
