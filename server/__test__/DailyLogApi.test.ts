import * as request from 'supertest'
import * as Knex from 'knex'
import App from '../app'

let token = ''
let logId: number = null

beforeEach(async () => {
  await request(App)
    .post('/auth/signup')
    .send({
      fullName: 'Test User',
      email: 'test1@example.com',
      password: 'test12345'
    })

  await request(App)
    .post('/auth/signin')
    .send({
      username: 'test1',
      password: 'test12345'
    })
    .then(res => {
      token = res.body.token
    })
})

describe('GET all the logs', () => {
  it('should return success/200', async done => {
    request(App)
      .get('/api/v1/daily')
      .set('Authorization', `Bearer ${token}`)
      .then(res => {
        expect(res.status).toEqual(200)
        done()
      })
  })
})

describe('Create a new daily log', () => {
  it('should create a new log', async done => {
    request(App)
      .post('/api/v1/daily')
      .set('Authorization', `Bearer ${token}`)
      .send({
        title: 'Task Title',
        description: 'short description',
        type: 0,
        status: 0,
        signifier: 0,
        date: new Date()
      })
      .then(res => {
        expect(res.status).toEqual(201)
        logId = res.body.data.id
        done()
      })
  })

})

describe('Delete the log', () => {
  it('should update the status of a log', async done => {
    request(App)
      .delete(`/api/v1/daily/${logId}`)
      .set('Authorization', `Bearer ${token}`)
      .then(res => {
        expect(res.status).toEqual(200)
        done()
      })
  })
})