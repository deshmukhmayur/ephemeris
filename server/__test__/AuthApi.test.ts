import * as request from 'supertest'
import * as Knex from 'knex'
import App from '../app'

const config = require('../knexfile')['test']
const knex = Knex(config)

const randomUsername = 'test' + Math.round(Math.random() * 100)
const randomPassword = 'Password' + Math.round(Math.random() * 100)

afterAll(async () => {
  await knex.raw(`delete from users where username='${randomUsername}'`)
})

describe('SignUp test', () => {  
  it('should return success on successful signup', done => {
    request(App)
      .post('/auth/signup')
      .send({
        fullName: 'Test User',
        email: randomUsername + '@example.com',
        password: randomPassword
      })
      .then(res => {
        expect(res.status).toEqual(201)
        expect(res.body.data.email).toEqual(randomUsername + '@example.com')
        done()
      })
  })

  it('should return error if user already exists', done => {
    request(App)
      .post('/auth/signup')
      .send({
        fullName: 'Test User',
        email: randomUsername + '@example.com',
        password: randomPassword
      })
      .then(res => {
        expect(res.status).toEqual(409)
        expect(res.body.error).toBeDefined()
        done()
      })
  })
})

describe('SignIn test', () => {
  it('should return success on successful login', done => {
    request(App)
      .post('/auth/signin')
      .send({
        username: randomUsername,
        password: randomPassword
      })
      .then(res => {
        expect(res.status).toEqual(200)
        expect(res.body.token).toBeDefined()
        expect(typeof res.body.token).toEqual(typeof 'string')
        done()
      })
  })

  it('should return error on incorrect username or password', done => {
    request(App)
      .post('/auth/signin')
      .send({
        username: 'incorrectuser',
        password: 'incorrectpasswd'
      })
      .then(res => {
        expect(res.status).toEqual(400)
        done()
      })
  })
})

// describe('fb login', () => {
//   it('should return success on successful login', done => {
//     request(App)
//       .post('/auth/fbsignin')
//       .send({
//         username: 'user',
//         password: 'passwd'
//       })
//       .then(res => {
//         expect(res.status).toEqual(200)
//         expect(res.body.message).toEqual('fb login')
//         done()
//       })
//   })
// })