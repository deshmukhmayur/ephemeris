import * as express from 'express'
import * as passport from 'passport'
import * as passportJWT from 'passport-jwt'
import { User } from './models'

const ExtractJwt = passportJWT.ExtractJwt
const JWTStrategy = passportJWT.Strategy

export const jwtOptions: passportJWT.StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
  passReqToCallback: true
}

passport.use(new JWTStrategy(jwtOptions, (req: express.Request, jwtPayload: any, done: passportJWT.VerifiedCallback) => {
  // console.log(req, jwtPayload, done)
  // console.log(jwtPayload.clientAddress, req.ip)
  if (jwtPayload.clientAddress !== req.ip) {
    return done(null, false)
  }
  User.query().findById(jwtPayload.id)
    .then(user => {
      return done(null, user)
    })
    .catch(err => {
      return done(err, false)
    })

}))