const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  entry: {
    "client": __dirname + "/client/index.tsx",
  },
  output: {
    filename: "scripts/bundle.js",
    path: __dirname + '/dist/client',
    publicPath: '/',
  },

  // enable sourcemaps for debugging webpack's output
  devtool: "source-map",

  resolve: {
    // Add .tsx? as resolvable extensions
    extensions: [".ts", ".tsx", ".js", ".json"]
  },

  module: {
    rules: [
      // All files with .tsx? extension will be handled by 'ts-loader'
      {
        test: /\.tsx?$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              babelrc: true,
              plugins: ['react-hot-loader/babel'],
            },
          },
          'ts-loader'
        ]
      },

      { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },

      { test: /\.css$/, use: ['style-loader', 'css-loader'] },

      {
        // Exclude `js` files to keep "css" loader working as it injects
        // it's runtime that would otherwise processed through "file" loader.
        // Also exclude `html` and `json` extensions so they get processed
        // by webpacks internal loaders.
        test: /\.(png|jpeg|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'static/media/[name].[hash:8].[ext]',
            },
          },
        ],
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: __dirname + '/public/index.html',
      chunks: ['client']
    }),
    new CopyWebpackPlugin([
      { from: '*', to: '', context: __dirname + '/public', ignore: 'index.html' },
    ]),
  ],

  devServer: {
    contentBase: __dirname + '/dist/client',
    compress: true,
    port: 3000,
    historyApiFallback: true,
  },

  // excludes react and react-dom from bundle to reduce the bundle size
  // externals: {
  //     "react": "React",
  //     "react-dom": "ReactDOM",
  //     "react-router-dom": 'ReactRouterDOM'
  // },
};