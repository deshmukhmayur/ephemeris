# Ephemeris

Ephemeris is a digital bullet journaling web app.
It can be your to-do list, sketchbook, notebook, diary, or all of the above!

Read this [wiki](https://bitbucket.org/deshmukhmayur/ephemeris/wiki) for more details.

### What is a Bullet Journal?

A Bullet Journal&reg; is a customizable and forgiving analog organization system developer by [Ryder Caroll](http://rydercarroll.com/).

All rights to Bullet Journal&reg; and BuJo&reg; are reserved by Lightcage, LLC. (https://bulletjournal.com)


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- [Node JS](https://nodejs.org/) (v8+) - A JavaScript runtime
- Linux/Mac OS is preferred, although it can work on windows with some changes

### Installing

After cloning this repository, install the dependencies
```sh
$ npm install
```

To start the dev server
```sh
$ npm start
```

The application should now be hosted on [`http://localhost:3000/`](http://localhost:3000/)

# Configuration

After installing the dependencies, you need to configure the project with a database, and other environment settings.

Copy the `server/.env.example` and name it `server/.env`
```sh
$ cp server/.env.example server/.env
```

Then replace the database, username and password fields with your own database values, and assign some JWT secret for authentication tokens.


## Deployment

Steps to deploy the web app on a server

1. Build the app
```sh
$ npm run build:prod
```

2. This bundles the production bundle in the `dist/` directory. Copy the contents of this directory to the root of the web server.

3. Migrate the database tables (make sure that the database mentioned in the knexfile.production is already present)
```sh
$ npm run migrate:prod
```


## Built With

- [React](https://reactjs.org/) - A JavaScript library for building user interfaces
- [Webpack](https://webpack.js.org/) - A static module bundler for modern JavaScript applications
- [TypeScript](https://www.typescriptlang.org) - A typed superset of JavaScript that compiles to plain JavaScript


## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](./LICENSE.md) file for details.