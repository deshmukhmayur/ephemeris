import * as fs from 'fs'
import * as path from 'path'

const staticAssets = [
    '.env'
]

staticAssets.forEach(filePath => {
    const fileName = path.basename(filePath)
    const dirName = path.dirname(filePath)

    // console.log(path.resolve('server', dirName, fileName), path.resolve('dist/server', dirName, fileName))
    fs.copyFileSync(path.resolve('server', dirName, fileName), path.resolve('dist/server', dirName, fileName))
})
