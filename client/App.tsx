import * as React from 'react'
import { Route, Link, Switch } from 'react-router-dom'
import { hot } from 'react-hot-loader'

import './styles.css'

import Splash from './pages/Auth/Splash'
import Signup from './pages/Auth/Signup'
import Login from './pages/Auth/Login'
import NotFound from './pages/NotFound/NotFound'
import Index from './pages/Index'
import Overview from './pages/Overview'
import DailyLog from './pages/DailyLog'
import MonthlyLog from './pages/MonthlyLog'

class App extends React.Component {
    render() {
        return (
                <Switch>
                    <Route exact path="/" component={Splash} />
                    <Route path="/signup" component={Signup} />
                    <Route path="/login" component={Login} />
                    <Route path="/index" component={Index} />
                    <Route path="/overview" component={Overview} />
                    <Route path="/daily-log" component={DailyLog} />
                    <Route path="/monthly-log" component={MonthlyLog} />
                    <Route path="**" component={NotFound} />
                </Switch>
        )
    }
}

export default hot(module)(App)