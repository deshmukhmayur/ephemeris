import * as React from 'react'

import './Page.css'

export default class Page extends React.Component {
  render() {
    return (
      <section className="page">
        {this.props.children}
      </section>
    )
  }
}