import * as React from 'react'
import { Link } from 'react-router-dom'

import './Header.css'

interface HeaderProps extends React.Props<any> {
  title: string
}

export default class Header extends React.Component<HeaderProps> {
  render() {
    return (
      <header>
        <Link to="/">e</Link>
        {this.props.title}
        <Link to="/help"><i className="ion-help-buoy" /></Link>
      </header>
    )
  }
}