import * as React from 'react'

interface BulletProps extends React.Props<any> {
  bullet: any,
  key: number
}

export default class Bullet extends React.Component<BulletProps> {
  signifier() {
    const SIGNIFIERS = {
      none: 0,
      important: 1,
      inspiration: 2,
      explore: 3,
    }
    switch (this.props.bullet.signifier) {
      case SIGNIFIERS.important:
        return <i className="ion-ios-star-outline" />
      case SIGNIFIERS.inspiration:
        return <i className="ion-ios-lightbulb-outline" />
      case SIGNIFIERS.explore:
        return <i className="ion-ios-eye-outline" />
      default:
        return <i>&#183;</i>
    }
  }

  status() {
    const TYPE = {
      task: 0,
      event: 1,
      note: 2
    }
    const STATUS = {
      none: 0,
      complete: 1,
      migrated: 2,
      scheduled: 3
    }

    if (this.props.bullet.type === TYPE.task) {
      switch (this.props.bullet.status) {
        case STATUS.complete:
          return <i className="ion-close-round" />
        case STATUS.migrated:
          return <i className="ion-ios-arrow-right" />
        case STATUS.scheduled:
          return <i className="ion-ios-arrow-back" />
        default:
          return <i>&#8226;</i>
      }
    } else if (this.props.bullet.type === TYPE.event) {
      
    } else if (this.props.bullet.type === TYPE.note) {

    }
  }

  render() {
    return (
      <li key={this.props.bullet.id}>
        {this.signifier()}
        {this.status()}
        <span className="task-title">{this.props.bullet.title}</span>
        <i className="edit ion-android-create" />
      </li>
    )
  }
}