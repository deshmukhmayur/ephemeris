import * as React from 'react'

import Bullet from './Bullet'

import './BulletList.css'

interface BulletListProps {
  list: Array<{
    id: number,
    title: string,
    type: number,
    status: number,
    signifier: number
  }>
}

export default class BulletList extends React.Component<BulletListProps> {
  render() {
    return (
      <ul className="bullet-list">
        {this.props.list.map((bullet, i) => {
          return <Bullet key={i} bullet={bullet} />
        })}
      </ul>
    )
  }
}