import * as React from 'react'
import { NavLink } from 'react-router-dom'

import './Toolbar.css'

export default class Toolbar extends React.Component {
  render() {
    return (
      <nav className="footer-nav">
        <NavLink to="/index" title="Index" activeClassName="active">
          <i className="ion-android-menu" />
        </NavLink>
        <NavLink to="/overview" title="Overview" activeClassName="active">
          <i className="ion-compass" />
        </NavLink>
        <NavLink to="/daily-log" title="Daily Log" activeClassName="active">
          <i className="ion-android-calendar" />
        </NavLink>
        <NavLink to="/monthly-log" title="Monthly Log" activeClassName="active">
          <i className="ion-ios-calendar-outline" />
        </NavLink>
      </nav>
    )
  }
}