import * as React from 'react'
import { Link } from 'react-router-dom'

import BulletList from '../components/BulletList'
import Header from '../components/Header'
import Page from '../components/Page'
import Toolbar from '../components/Toolbar'

export default class DailyLog extends React.Component {
  render() {
    const list = [
      {
        id: 1,
        signifier: 1,
        status: 0,
        title: 'Task #1',
        type: 0,
      },
      {
        id: 2,
        signifier: 0,
        status: 0,
        title: 'Task #2',
        type: 0,
      },
      {
        id: 3,
        signifier: 3,
        status: 0,
        title: 'Task #3',
        type: 0,
      },
      {
        id: 4,
        signifier: 2,
        status: 0,
        title: 'Task #4',
        type: 0,
      }
    ]
    return (
      <>
        <Header title="Daily Log" />
        <Page>
          <div>
            <h3 className="type--title" style={{ display: 'flex' }}>
              <Link to="#">
                <i className="ion-android-calendar black-text" />
              </Link>
              <span className="separator" />
              March, 20
            <span className="separator" />
              <Link to="#">
                <i className="ion-android-arrow-dropdown black-text" />
              </Link>
            </h3>
          </div>

          <main>
            <BulletList list={list} />
          </main>
        </Page>
        <footer>
          <Toolbar />
        </footer>
      </>
    )
  }
}