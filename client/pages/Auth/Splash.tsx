import * as React from 'react'
import { Link } from 'react-router-dom'

import Page from '../../components/Page'

export default class Login extends React.Component {
  render() {
    return (
      <Page>
        <main className="text-center valign-center">
          <h1 className="logo type--display4 m-0">e</h1>
          <p>organize you day</p>

          <div>
            <Link to="/#" className="btn btn-outline btn-block primary-text white m-b-8">CONTINUE WITH FACEBOOK</Link>
            <Link to="/signup" className="btn btn-outline btn-block black-text m-b-8">SIGNUP WITH EMAIL</Link>
            <p>or</p>
            <Link to="/login" className="btn btn-outline btn-block black-text m-b-8">LOGIN</Link>
          </div>
        </main>
      </Page>
    )
  }
}
