import * as React from 'react'
import { Link } from 'react-router-dom'

import Page from '../../components/Page'

export default class Login extends React.Component {
  render() {
    return (
      <Page>
        <main className="text-center valign-center">
          <form method="post">
            <h3 className="type--headline type--relaxed m-0 m-b-24">SIGNUP</h3>

            <input type="email" name="email" id="email" placeholder="email" className="m-b-8" />
            <input type="password" name="password" id="password" placeholder="password" className="m-b-8" />
            <input type="password" name="password2" id="password2" placeholder="re-enter password" className="m-b-8" />

            <p className="type--caption">By clicking on Sign Up, you agree to the Terms of Service, Privacy Policy</p>

            <button type="submit" className="btn btn-block accent white-text m-b-24">SIGNUP</button>

            <Link to="#" className="btn btn-block primary-text white m-b-16">CONTINUE WITH FACEBOOK</Link>
            <p className="type--caption m-0">Already registered?</p>
            <Link to="/login" className="btn btn-block white black-text">LOGIN</Link>
          </form>
        </main>
      </Page>
    )
  }
}