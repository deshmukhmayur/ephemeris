import * as React from 'react'
import { Link } from 'react-router-dom'

import Page from '../../components/Page'

export default class Login extends React.Component {
  render() {
    return (
      <Page>
        <main className="text-center valign-center">
          <form action="/daily-log">
            <h3 className="type--headline type--relaxed m-0 m-b-24">LOGIN</h3>

            <input type="email" name="email" id="email" placeholder="email" className="m-b-8" />
            <input type="password" name="password" id="password" placeholder="password" className="m-b-8" />

            <div className="type--caption m-b-8">
              <input type="checkbox" name="remember" id="remember" style={{ width: '0.9em' }} />
              <label htmlFor="remember">Remember Me</label>
            </div>

            <button type="submit" className="btn btn-block accent white m-b-8">LOGIN</button>
            <Link to="#" className="primary-text type--caption m-b-24" style={{ display: 'inline-block' }}>Forgot Password?</Link>

            <Link to="#" className="btn btn-block primary-text white m-b-16">CONTINUE WITH FACEBOOK</Link>
            <p className="type--caption m-0">Don't have an account?</p>
            <Link to="/signup" className="btn btn-block white black-text">SIGNUP WITH EMAIL</Link>
          </form>
        </main>
      </Page>
    )
  }
}