import * as React from 'react'
import { Link } from 'react-router-dom'

import Page from '../../components/Page'

export default class NotFound extends React.Component {
  render() {
    return (
      <Page>
        <main className="text-center valign-center">
          <h1 className="type--headline">Page Not Found</h1>

          <p className="type--body">It seems that you have lost your way...</p>

          <Link to="/" className="btn btn-outline">TAKE ME HOME</Link>
        </main>
      </Page>
    )
  }
}