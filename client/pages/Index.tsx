import * as React from 'react'

import Header from '../components/Header'
import Page from '../components/Page'
import Toolbar from '../components/Toolbar'

export default class Index extends React.Component {
  render() {
    return (
      <>
        <Header title="Index" />
        <Page>
          <main className="text-center valign-center">
            <p>Nothing to see here, yet</p>
          </main>
        </Page>
        <footer>
          <Toolbar />
        </footer>
      </>
    )
  }
}